### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

This repository aim to helps illustrate how forks are working.

### Welcome to the team

I'm proud to tell you that the team made an awsome project!
I let you discover what has been done bye the team.

If you have any comments feel free to create issues for discussion.

LGO -> Modification du readmeExercice Module 3 lab 1
